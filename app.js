const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const mysql = require('mysql2/promise');
require('dotenv').config();

const productRoutes = require('./api/routes/products');
const trainingRoutes = require('./api/routes/trainings');
const mealRoutes = require('./api/routes/meals');
const userRoutes = require('./api/routes/user');
const measurementRoutes = require('./api/routes/measurements');

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/static', express.static('static'));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Credentials", "true");
  if (req.method === 'OPTIONS') {
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    return res.status(200).json({});
  }
  next();
});

app.use(async (req, res, next) => {
  res.db = await mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME
  });
  next();
});

app.use('/products', productRoutes);
app.use('/trainings', trainingRoutes);
app.use('/user', userRoutes);
app.use('/meals', mealRoutes);
app.use('/meas', measurementRoutes);

app.use((req, res, next) => {
  const error = new Error('Inappropriate request');
  error.status = 404;
  next(error);
});

app.use((err, req, res, next) => {
  // console.log(err)
  console.error(err.stack);
  res.status(err.status || 500);
  res.json({
    msg: err.message || 'Undefined error'
  });
});


module.exports = app;
