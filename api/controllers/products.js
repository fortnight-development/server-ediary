const axios = require('axios');
const request = require('request');
const cheerio = require('cheerio');
const removeAccents = require('remove-accents');
const fs = require('fs-extra');
const Tesseract = require('tesseract.js');

const dbActionHandler = require('../middleware/db-action-handler');


const undefinedToNull = array => array.map(val => val == null ? null : val);

const getHTML = (url, method = 'GET', headers = {}, form = {}) => {
  const start = new Date().getTime();
  return new Promise((resolve, reject) => {
    request({ url, method, headers, form }, (err, res, html) => {
      if (err) throw err;
      const $ = cheerio.load(html);
      const end = new Date().getTime();
      console.log(`HTML fetched in: ${end-start} ms`);
      $ ? resolve($) : reject({ msg: 'Html could not be fetched', code: 400, err });
    });
  });
}

const getNumber = val => {
  const replaceComma = val.replace(/,/g, '.');
  return parseFloat(replaceComma.replace(/^\D+/g, ''));
}

exports.get_products = async (req, res, next) => {
  const { productName } = req.params;
  const { userId } = req.userData;
  const sql = `SELECT p.id, IF(p.name IS NULL, p.own_name, p.name) as name, p.carbs, p.prots, p.fats, p.kcals, p.portion_weight portionWeight, p.img_url img, p.producer, p.ingredients, ps.name 'source'
    FROM dj__products p
    INNER JOIN dj__products_sources ps ON p.source = ps.id
    WHERE (p.name LIKE ? AND p.confirmed = TRUE) OR (p.own_name LIKE ? AND p.user_id = ?) LIMIT 10`;
  const [ getProductsDb ] = await res.db.execute(sql, [`%${productName}%`, `%${productName}%`, userId]);
  const productsDb = getProductsDb.map(product => ({...product, saved: true }));
  // console.log(productsDb)

  const ilewazyUrl = `http://www.ilewazy.pl/ajax/load-products/ppage/10/keyword/${productName}`;
  const getProductsIlewazy = await axios.get(ilewazyUrl, {
    headers: { 'X-Requested-With':'XMLHttpRequest' } 
  });

  if (!getProductsIlewazy.data.data) return { products: productsDb };

  const productsIlewazy = getProductsIlewazy.data.data.map(product => {
    const firstUnit = product.unitdata[Object.keys(product.unitdata)[0]];
    return {
      name: product.ingredient_name.trim(),
      producer: null,
      carbs: parseFloat(product.weglowodany),
      prots: parseFloat(product.bialko),
      fats: parseFloat(product.tluszcz),
      kcals: parseFloat(product.energia),
      portionWeight: product.weight ? parseFloat(product.weight) : parseFloat(firstUnit.unit_weight),
      img: `http://static.ilewazy.pl/wp-content/uploads/${firstUnit.filename}`,
      imgName: firstUnit.filename,
      saved: false,
      source: 'ilewazy'
    }
  });

  const uniqueProducts = productsIlewazy.reduce((uniqueProducts, product) => {
    if (productsDb.some(prod => prod.name == product.name)) {
      return uniqueProducts;
    } else {
      uniqueProducts.push(product);
      return uniqueProducts;
    }
  }, []);

  const products = [...productsDb, ...uniqueProducts];
  return { records: products.length, products };
}

exports.post_product = async (req, res, next) => {
  const { name, producer, carbs, prots, fats, kcals, portionWeight, ingredients, img, source, imgName } = req.body;
  const { userId } = req.userData;

  const [findProduct] = await res.db.execute('SELECT id FROM dj__products WHERE name = ? AND confirmed = TRUE', [name]);
  if (findProduct.length) return findProduct[0];

  const saveProductImage = async (imgUrl, imgName) => {
    const getImage = await axios.get(imgUrl, { responseType: 'arraybuffer' });
    const imgLocalUrl = `static/img/products/${imgName}`;
    await fs.writeFile(imgLocalUrl, getImage.data);
    return imgLocalUrl;
  }

  const imgLocalUrl = (source == 'ilewazy') ? await saveProductImage(img, imgName) : (req.file && req.file.path ? req.file.path : null);

  const sqlColName = source == 'ilewazy' ? 'name' : 'own_name';
  const sql = `INSERT INTO dj__products (${sqlColName}, user_id, confirmed, producer, carbs, prots, fats, kcals, portion_weight, ingredients, img_url, ${`source`})
    SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ps.id
    FROM dj__products_sources ps
    WHERE ps.name = ?`;

  const sourceBasedVals = source => source == 'ilewazy' ? [null, true] : [userId, false]; 
  const sqlVals = [name, ...sourceBasedVals(source), producer, carbs, prots, fats, kcals, portionWeight, ingredients, imgLocalUrl, source];

  const [postProduct] = await res.db.execute(sql, undefinedToNull(sqlVals));
  return await dbActionHandler(postProduct, `Product with name ${name}`);
}

exports.post_image_recognition = async (req, res, next) => {
  const tesseractPromise = await Tesseract
    .create({ langPath: "./pol.traineddata" })
    .recognize(req.file.path, "pol");
  console.log(tesseractPromise)
  return { text: tesseractPromise.text };
}

exports.post_product_ilewazy = async (req, res, next) => {
  const { url } = req.body;
  const $ = await getHTML(url);

  const fetchProduct = async $ => {
    const detailsDiv = $('.span10').children('table').children('tbody');
    const macroTab = $('#ilewazy-ingedients');
  
    const getMacroVal = contains => getNumber(macroTab.find(`.nutrition-label:contains(${contains})`).next().text().trim());
    const getProductDetails = contains => detailsDiv.find(`.product-data-label:contains(${contains})`).next().text().trim() || undefined;
  
    const imgUrl = $('.main-img').attr('src');
    const name = $('.weighting-title').text().trim();
    let imgName = removeAccents(name).replace(/\s+/g, '-').replace(/,/g, '').toLowerCase();
        imgName = 'static/img/products/' + imgName + '.jpg';

    request
      .get(imgUrl)
      .on('error', err => { throw err })
      .pipe(fs.createWriteStream(imgName))
  
    return {
      name,
      producer: getProductDetails('Pro'),
      brand: getProductDetails('Mar'),
      ingredients: getProductDetails('Skł'),
      portionWeight: Number(macroTab.find('#current-unit-weight').text().trim()),
      carbs: getMacroVal('Węgl'),
      prots: getMacroVal('Biał'),
      fats: getMacroVal('Tłus'),
      kcals: getMacroVal('Ener'),
      img: imgName,
      source: 'ilewazy',
      fetched: true,
      status: {
        fetched: true,
        confirmed: true
      }
    }
  }

  const sql = `INSERT INTO dj__products 
    (id, name, own_name, producer, carbs, prots, fats, kcals, portion_weight, confirmed, user_id, img_url, ingredients, source) 
    VALUES (NULL, ?, NULL, ?, ?, ?, ?, ?, ?, TRUE, NULL, ?, ?, ?)`;

  const p = await fetchProduct($);
  const postProduct = await res.db.execute(sql, [p.name, p.producer || null, p.carbs, p.prots, p.fats, p.kcals, p.portionWeight, p.img, p.ingredients || null, p.source]);

  return {
    status: 201,
    product: {
      id: postProduct[0].insertId,
      ...p,
      status: undefined
    } 
  }
}