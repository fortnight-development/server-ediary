const camelCase = require('camelcase');
const moment = require('moment');
const dbActionHandler = require('./../middleware/db-action-handler');

const checkPerms = (userId, groups, meal) => new Promise((resolve, reject) => {
	if ((meal.createdBy == userId) || (meal.meta.canEdit.includes(userId)) || (groups.includes('Admin'))) {
		resolve();
	} else {
		reject({ msg: 'You are not allowed to make any changes for this resource', status: 401 });
	}
});

const extract = (phrase, objToExtract) => Object.keys(objToExtract)
	.map(key => key.split('_'))
	.filter(key => key[0]==phrase)  
	.reduce((obj, props) => ({ 
		...obj,
		[props[1]]: objToExtract[`${props[0]}_${props[1]}`]
	}), {});

const undefinedToNULL = array => array.map(val => val==null ? null : val);

exports.get_meals_date = async (req, res, next) => {
	const { mealDate } = req.params;
	const { userId } = req.userData;
	const momentDate = moment(mealDate, 'YYYY-MM-DD');

	const startDate = momentDate.add(-2, 'day').format('YYYY-MM-DD');
	const endDate = moment(startDate, 'YYYY-MM-DD').add(7, 'day').format('YYYY-MM-DD');
	
	const getMealsSql = `SELECT m.id, m.name, m.date, m.carbs, m.prots, m.fats, m.kcals, m.created_at createdAt FROM dj__meals m
		WHERE m.date >= ? AND m.date < ? AND m.user_id = ?`;

	const getProductsSql = `SELECT m.id mealId, p.id, p.name, p.carbs, p.prots, p.fats, p.kcals, mp.product_portion_weight portionWeight, p.img_url img, p.source, p.producer, p.ingredients FROM dj__meals_products mp
		INNER JOIN dj__meals m ON mp.meal_id = m.id
		INNER JOIN dj__products p ON mp.product_id = p.id
		WHERE m.date >= ? AND m.date < ? AND m.user_id = ?`;

	const sqlVals = [`${startDate} 00:00:00`, `${endDate} 00:00:00`, userId];

	const [[meals], [products]] = await Promise.all([
		res.db.execute(getMealsSql, sqlVals),
		res.db.execute(getProductsSql, sqlVals)
	]);
	
	if (!meals.length) return { meals: [] };

	const assignProductsToMeals = meals.map(meal => {
		meal.products = products.filter(product => product.mealId == meal.id) || [];
		return meal;
	});
	
	return { meals: assignProductsToMeals };
}

exports.post_meal = async (req, res, next) => {
	const { name, date } = req.body;
	const { carbs, prots, fats, kcals, recipe, public } = req.body;
	const { userId } = req.userData;

	const sql = `INSERT INTO dj__meals(id, name, date, carbs, prots, fats, kcals, recipe, public, user_id)
		VALUES (NULL, ?, ?, ?, ?, ?, ?, IFNULL(?, recipe), IFNULL(?, public), ?)`;
	const sqlVals = undefinedToNULL([name, date, carbs, prots, fats, kcals, recipe, public, userId]);

	const [postMeal] = await res.db.execute(sql, sqlVals);

	return await dbActionHandler(postMeal, `Meal with name ${name}`);
	// return { msg: `Meal was successfully posted with id ${postMeal[0].insertId}`, mealId: postMeal[0].insertId }
}

exports.patch_meal = async (req, res, next) => {
	const { name, date, public, recipe } = req.body;
	const { carbs, prots, fats, kcals } = req.body;
	const { mealId } = req.params;
	const { userId } = req.userData;

	const updateProps = undefinedToNULL([name, date, carbs, prots, fats, kcals, recipe, public, mealId, userId]);
	const sql = `UPDATE dj__meals m SET name = COALESCE(?, name), date = COALESCE(?, date), carbs = COALESCE(?, carbs), prots = COALESCE(?, prots), fats = COALESCE(?, fats), kcals = COALESCE(?, kcals), recipe = COALESCE(?, recipe), public = COALESCE(?, public)
		WHERE m.id= ? AND m.user_id= ?`;
	
	const patchMeal = await res.db.execute(sql, updateProps);

	if (patchMeal[0].affectedRows == 0) {
		return { msg: `Meal ${mealId} doesn't exist or you have no permissions for patching it`, status: 403 }
	}	else if (patchMeal[0].changedRows == 0) {
		return { msg: 'Nothing was changed', status: 202 }
	} else {
		return { msg: `Meal ${mealId} was succesfully patched` }
	}
}

exports.delete_meal = async (req, res, next) => {
	const { mealId } = req.params;
	const { userId } = req.userData;

	const sql = `DELETE m.*, mp.* FROM dj__meals m
		LEFT JOIN dj__meals_products mp ON m.id = mp.meal_id
		WHERE m.id= ? AND m.user_id= ?`;
	const deleteMeal = await res.db.execute(sql, [mealId, userId]);

	if (deleteMeal[0].affectedRows == 0) {
		return { msg: `Meal ${mealId} doesn't exist or you have no permissions for deleting it`, status: 404 }
	} else {
		return { msg: `Meal ${mealId} was successfully deleted` }
	}
}

exports.post_meal_product = async (req, res, next) => {
	const { mealId, productId } = req.params;
	const { portionWeight } = req.body;
	console.log(req.body)
	const sql = 'INSERT INTO dj__meals_products (id, meal_id, product_id, product_portion_weight) VALUES (NULL, ?, ?, ?)';
	await res.db.execute(sql, [mealId, productId, portionWeight]);

	return { msg: `Product ${productId} was successfully added to meal ${mealId}`}
}

exports.patch_meal_product = async (req, res, next) => {
	const { mealId, productId } = req.params;
	const { portionWeight } = req.body;
	const { userId } = req.userData;

	const sqlVals = [portionWeight, productId, mealId, userId];
	console.log(sqlVals.length)


	const sql = `UPDATE dj__meals_products AS mp
		INNER JOIN dj__meals AS m ON mp.meal_id=m.id
		SET mp.product_portion_weight= ?
		WHERE mp.product_id= ? AND mp.meal_id= ? AND m.user_id= ?`;

	const patchMealProduct = await res.db.execute(sql, [portionWeight, productId, mealId, userId]);
	
	if (patchMealProduct[0].affectedRows == 0) {
		return { msg: `Product ${productId} for meal ${mealId} doesn't exist or you have no permissions for patching it`, status: 404 }
	} else if (patchMealProduct[0].changedRows == 0) {
		return { msg: `Nothing was changed`, status: 202 }
	} else {
		return { msg: `Product ${productId} was succsefully patched for meal ${mealId}` }
	}
}

exports.delete_meal_product = async (req, res, next) => {
	const { mealId, productId } = req.params;
	const { userId } = req.userData;

	const sql = `DELETE mp.* FROM dj__meals_products AS mp
		INNER JOIN dj__meals AS m ON mp.meal_id=m.id
		WHERE mp.meal_id=? AND mp.product_id=? AND m.user_id=?`;
	
	const deleteMealProduct = await res.db.execute(sql, [mealId, productId, userId]);

	if (deleteMealProduct[0].affectedRows == 0) {
		return { msg: `Product ${productId} for meal ${mealId} doesn't exist or you have no permissions for deleting it`, status: 404 }
	} else {
		return { msg: `Product ${productId} was successfully deleted for meal ${mealId}` }
	}
}