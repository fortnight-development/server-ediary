const dbActionHandler = require('../middleware/db-action-handler');
const moment = require('moment');

const undefinedToNull = array => array.map(prop => (prop == null) ? null : prop);

// Opcjonalnie inner join z exercise name, zamiast podawania id można podać nazwę
exports.get_exercises = async (req, res, next) => {
	const { exerciseDate } = req.params;
	const { userId } = req.userData;
	const addWeek = moment(exerciseDate, 'YYYY-MM-DD').add(1, 'week').format('YYYY-MM-DD');
	const startDate = `${exerciseDate} 00:00:00`;
	const endDate = `${addWeek} 00:00:00`;

	const exercSql = `SELECT e.id, e.name nameId, en.name name, e.date, e.duration, e.break FROM tr__exercises e
		INNER JOIN tr__exercises_names en ON e.name = en.id
		WHERE e.user_id = ? AND e.date >= ? AND e.date < ?`;

	const setsSql = `SELECT es.id, es.exercise_id exerciseId, es.repeats, es.loadweight, es.time, es.break FROM tr__exercises_sets es
		INNER JOIN tr__exercises e ON es.exercise_id = e.id
		INNER JOIN tr__exercises_names en ON en.id = e.name
		WHERE e.user_id = ? AND e.date >= ? AND e.date < ?`;

	const [[getExercises], [getSets]] = await Promise.all([
		res.db.execute(exercSql, [userId, startDate, endDate]),
		res.db.execute(setsSql, [userId, startDate, endDate])
	]);

	const exercises = getExercises.map(exercise => ({
		...exercise,
		sets: getSets.filter(set => set.exerciseId == exercise.id) || []
	}));
	
	return { exercises };
}


exports.post_exercise = async (req, res, next) => {
	const { nameId, date, duration, break: breakTime } = req.body;
	const { userId } = req.userData;
	const sql = `INSERT INTO tr__exercises (name, date, user_id, duration, break)
		SELECT en.id, IFNULL(?, CURRENT_TIMESTAMP), ?, ?, ? FROM tr__exercises_names en
		WHERE en.id = ? AND (en.user_id = ? OR en.user_id IS NULL)`;
	const [postExercise] = await res.db.execute(sql, [date || null, userId, duration, breakTime || null, nameId, userId]);
	return await dbActionHandler(postExercise, `Exercise with name id ${nameId}`);
}

exports.patch_exercise = async (req, res, next) => {
	const { exerciseId } = req.params;
	const { nameId, date, duration, break: breakTime } = req.body;
	const { userId } = req.userData;
	const sql = `UPDATE tr__exercises e
		SET e.name = IF((SELECT COUNT(*) FROM tr__exercises_names en WHERE en.id = ? AND (en.user_id = e.user_id OR en.user_id IS NULL)) <> 0, ?, e.name),
		e.date = COALESCE(?, e.date), e.duration = COALESCE(?, e.duration), ${breakTime === undefined ? ' e.break = e.break' : ' e.break = ?'}
		WHERE e.id = ? AND e.user_id = ?`;
	const [patchExercise] = await res.db.execute(sql, undefinedToNull([nameId, nameId, date, duration, breakTime, exerciseId, userId]));
	return await dbActionHandler(patchExercise, `Exercise with id ${exerciseId}`, 'patch');
}

exports.delete_exercise = async (req, res, next) => {
	const { exerciseId } = req.params;
	const { userId } = req.userData;
	const sql = `DELETE e.*, es.* FROM tr__exercises e 
		LEFT JOIN tr__exercises_sets es ON e.id = es.exercise_id
		WHERE e.id = ? AND e.user_id = ?`;
	const [deleteExercise] = await res.db.execute(sql, [exerciseId, userId]);
	return await dbActionHandler(deleteExercise, `Exercise with id ${exerciseId}`, 'delete');
}


exports.post_exercise_set = async (req, res, next) => {
	const { exerciseId } = req.params;
	const { repeats, loadweight, time, break: breakTime } = req.body;
	const { userId } = req.userData;
	const sql = `INSERT INTO tr__exercises_sets (exercise_id, repeats, loadweight, time, break)
		SELECT e.id, ?, ?, ?, ? 
		FROM tr__exercises e
		WHERE e.id = ? AND e.user_id = ?`;
	const [postExerciseSet] = await res.db.execute(sql, [repeats, loadweight, time, breakTime, exerciseId, userId]);
	return await dbActionHandler(postExerciseSet, `Set for exercise with id ${exerciseId}`);
}

exports.patch_exercise_set = async (req, res, next) => {
	const { exerciseId, setId } = req.params;
	const { repeats, loadweight, time, break: breakTime } = req.body;
	const { userId } = req.userData;
	console.log(req.body)
	const sql = `UPDATE tr__exercises_sets es
		INNER JOIN tr__exercises e ON es.exercise_id = e.id
		SET repeats = COALESCE(?, repeats), loadweight = COALESCE(?, loadweight),
		time = COALESCE(?, time), es.break = COALESCE(?, e.break)
		WHERE e.id = ? AND es.id = ? AND e.user_id = ?`;

	const [patchExerciseSet] = await res.db.execute(sql, undefinedToNull([repeats, loadweight, time, breakTime, exerciseId, setId, userId]));
	return await dbActionHandler(patchExerciseSet, `Exercise set with id ${setId}`, 'patch');
}

exports.patch_exercise_all_sets = async (req, res, next) => {
	const { exerciseId } = req.params;
	const { sets } = req.body;
	const { userId } = req.userData;
	/*
	const prepValsToUpdate = valuesNames => valuesNames
		.reduce((statement, valName, index) => 
			statement += `${valName} = COALESCE(?, ${valName})${index == valuesNames.length - 1 ? ' ' : ', '}`, '');
	*/

	const sqlVals = [];
	let sql = '';

	sets.forEach(set => {
		const { repeats, loadweight, time, break: breakTime, id: setId } = set;
		sqlVals.push([repeats, loadweight, time, breakTime, exerciseId, setId, userId]);
		console.log(sqlVals);
		sql += `UPDATE tr__exercises_sets es
			INNER JOIN tr__exercises e ON es.exercise_id = e.id
			SET repeats = COALESCE(?, repeats), loadweight = COALESCE(?, loadweight),
			time = COALESCE(?, time), break = COALESCE(?, break)
			WHERE e.id = ? AND es.id = ? AND e.user_id = ? ;`;
	});

	const [patchExerciseAllSets] = await res.db.execute(sql, sqlVals);
	return patchExerciseAllSets;
}

exports.delete_exercise_set = async (req, res, next) => {
	const { exerciseId, setId } = req.params;
	const { userId } = req.userData;
	const sql = `DELETE es.* FROM tr__exercises_sets es
		INNER JOIN tr__exercises e ON es.exercise_id = e.id
		WHERE e.id = ? AND es.id = ? AND e.user_id = ?`;
	const [deleteExerciseSet] = await res.db.execute(sql, [exerciseId, setId, userId]);
	return await dbActionHandler(deleteExerciseSet, `Exercise set with id ${setId}`, 'delete');
}



exports.get_exercise_names = async (req, res, next) => {
	const { exerciseName } = req.params;
	const { userId } = req.userData;
	const sql = `SELECT en.id nameId, en.name FROM tr__exercises_names en
		WHERE en.name LIKE ? AND (en.user_id IS NULL OR en.user_id = ?) LIMIT 10`;
	const [getExerciseNames] = await res.db.execute(sql, [`%${exerciseName}%`, userId]);
	return { exerciseNames: getExerciseNames };
}

exports.post_exercise_name = async (req, res, next) => {
	const { name } = req.body.exercise;
	const { userId } = req.userData;
	const [postExerciseName] = await res.db.execute('INSERT INTO tr__exercises_names (name, user_id) VALUES (?, ?)', [name, userId]);
	return await dbActionHandler(postExerciseName, `Exercise name ${name}`, null, 'id');
}

exports.patch_exercise_name = async (req, res, next) => {
	const { exerciseNameId } = req.params;
	const { name } = req.body.exercise;
	const { userId } = req.userData;
	const [patchExerciseName] = await res.db.execute('UPDATE tr__exercises_names SET name = COALESCE(?, name) WHERE id = ? AND user_id = ?', [name, exerciseNameId, userId]);
	return await dbActionHandler(patchExerciseName, `Exercise name with id ${exerciseNameId}`, 'patch');
}

exports.delete_exercise_name = async (req, res, next) => {
	const { exerciseNameId } = req.params;
	const { userId } = req.userData;
	const [deleteExerciseName] = await res.db.execute('DELETE FROM tr__exercises_names WHERE id = ? AND user_id = ?', [exerciseNameId, userId]);
	return await dbActionHandler(deleteExerciseName, `Exercise name with id ${exerciseNameId}`, 'delete');
}