const dbActionHandler = require('../middleware/db-action-handler');

const undefinedToNull = array => array.map(prop => (prop == null) ? null : prop);

exports.get_general = async (req, res, next) => {
	const { userId } = req.userData;
	const generalSql = 'SELECT g.id, g.age, g.man, g.height FROM mr__general g WHERE user_id = ?';
	const allSql = `SELECT a.id, an.name, a.value, a.date FROM mr__all a
		INNER JOIN mr__all_names an ON a.name = an.id
		WHERE a.user_id = ? ORDER BY a.date DESC LIMIT 20`;
	const [[generalMeas], [allMeas]] = await Promise.all([
		res.db.execute(generalSql, [userId]),
		res.db.execute(allSql, [userId])
	]);
	if (!generalMeas.length) return { meas: [] };
		
		// generalMeas[0].all = allMeas;
	return {
		meas: {
			...generalMeas[0],
			all: allMeas
		}
	}
	// for (const meas of allMeas) {
	// 	generalMeas[0]['all'] = generalMeas[0]['all'] || {};
	// 	generalMeas[0]['all'][meas.name] = generalMeas[0]['all'][meas.name] || [];
	// 	generalMeas[0]['all'][meas.name].push(meas);
	// }
	return { meas: generalMeas[0] }
}

exports.post_general = async (req, res, next) => {
	const { age, man, height, weight, weightGoal } = req.body;
	const { userId } = req.userData;
	const sql1 = `INSERT INTO mr__general (age, man, height, weight_goal, user_id) VALUES (?, ?, ?, ?, ?)`;
	const sql2 = `INSERT INTO mr__all (name, value, user_id)
    SELECT an.id, ?, ?
    FROM mr__all_names an
    WHERE an.name = 'waga'`;

  await Promise.all([
  	res.db.execute(sql1, [age, Boolean(man), height, weightGoal, userId]),
  	res.db.execute(sql2, [weight, userId])
  ]);

  return { msg: `Data was succesfully saved` }
  // return await dbActionHandler() 
}

exports.patch_general = async (req, res, next) => {
	const { age, man, height, weight, weightGoal } = req.body;
	const { userId } = req.userData;
	const sql = `UPDATE mr__general
		SET age = COALESCE(?, age), man = COALESCE(?, man), height = COALESCE(?, height), weight_goal = COALESCE (?, weight_goal)
		WHERE user_id = ?`;
  const [updateGeneralMeas] = await res.db.execute(sql, undefinedToNull([age, man, height, weightGoal, userId]));
	return await dbActionHandler(updateGeneralMeas, `Measurement with these properties`, 'patch');
}



exports.post_all = async (req, res, next) => {
	const { nameId, date, value } = req.body;
	const { userId } = req.userData;
	const sql = `INSERT INTO mr__all (name, value, date, user_id)
		SELECT an.id, ?, IFNULL(?, CURRENT_TIMESTAMP), ?
		FROM mr__all_names an
		WHERE an.id = ? AND (an.user_id = ? OR an.user_id IS NULL)`;
	const [postAllMeas] = await res.db.execute(sql, [value, date || null, userId, nameId, userId]);
	return await dbActionHandler(postAllMeas,`Measurement with name id ${nameId}`,null,);
}

/// POPRAWIC .. ZE ZMIANĄ NAZWY PRZY UPDATOWANIU
// UPDATE -> INNER JOIN
exports.patch_all = async (req, res, next) => {
	const { id, nameId, value, date } = req.body.meas;
	const { userId } = req.userData;
	// const sql = `UPDATE mr__all SET value = COALESCE(?, value), date = COALESCE(?, date) WHERE id= ? AND user_id= ?`;
	const sql = `UPDATE mr__all a
		SET a.name = IF((SELECT COUNT(*) FROM mr__all_names an WHERE an.id = ? AND (an.user_id = a.user_id OR an.user_id IS NULL)) <> 0, ?, a.name)
		a.value = COALESCE(?, a.value), a.date = COALESCE(?, a.date)
		WHERE a.id= ? AND a.user_id= ?`;
	const [updateAllMeas] = await res.db.execute(sql, undefinedToNull([nameId, nameId, value, date || null,  id, userId]));
	return await dbActionHandler(updateAllMeas, `Measurement with id ${id}`, 'patch');
}

exports.delete_all = async (req, res, next) => {
	const { id } = req.body.meas;
	const { userId } = req.userData;
	const [deleteAllMeas] = await res.db.execute('DELETE FROM mr__all WHERE id = ? AND user_id = ?', [id, userId]);
	return await dbActionHandler(deleteAllMeas, `Measurement with id ${id}`, 'delete');
}



exports.post_all_name = async (req, res, next) => {
	const { name } = req.params.meas;
	const { userId } = req.userData;
  const [postMeasName] = await res.db.execute('INSERT INTO mr__all_names (name, user_id) (?, ?)', [name, userId]);
  return await dbActionHandler(postMeasName, `Measurement with name ${name}`, null);
}

exports.patch_all_name = async (req, res, next) => {
	const { name } = req.params.meas;
	const { measNameId } = req.params;
  const { userId } = req.userData;
  const sql = 'UPDATE mr__all_names SET name = ? WHERE id = ? user_id = ?';
  const [patchMeasName] = await res.db.execute(sql, [name, measNameId, userId]);
  return await dbActionHandler(patchMeasName, `Measurement name with id ${measNameId}`, 'patch');
}

exports.delete_all_name = async (req, res, next) => {
	const { measNameId } = req.params;
	const { userId } = req.userData;
  const [deleteMeasName] = await res.db.execute('DELETE FROM mr__all_names WHERE id = ? AND user_id = ?', [measNameId, userId]);
  return await dbActionHandler(deleteMeasName, `Measurement name with id ${measNameId}`, 'delete');
}
