const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const jwtSign = promisify(jwt.sign);
const ip = require('ip');
const moment = require('moment');
const handleDbAction = require('../middleware/db-action-handler');

const toNull = array => array.map(prop => (prop == null) ? null : prop);

const sortByDate = array => array.sort((a, b) => moment.utc(b.date).diff(moment.utc(a.date)));

const extract = (phrase, objToExtract) => Object.keys(objToExtract)
	.map(key => key.split('_'))
	.filter(key => key[0] == phrase)  
	.reduce((obj, props) => ({ 
		...obj,
		[props[1]]: objToExtract[`${props[0]}_${props[1]}`]
	}), {});

exports.post_register = async (req, res, next) => {
	const { email, password } = req.body;

	const [ findUser ] = await res.db.execute('SELECT * FROM u__accounts u WHERE u.email= ?', [email]);
	if (findUser.length) return { msg: `Email ${email} is already taken`, status: 409 }
	
	const hash = await bcrypt.hash(password, 10);

	const sql = 'INSERT INTO u__accounts (email, password) VALUES (?, ?)';
	const addUser = await res.db.execute(sql, [email, hash]);
	
	return {
		status: 202,
		msg: `User with email ${email} has been succesfully created`,
		user: {
			id: addUser[0].insertId,
			email
		}
	}
}

exports.post_login = async (req, res, next) => {
	const { email, password } = req.body;
	console.log(req.body)
	const authFail = { msg: 'Authentication failed', status: 401 };

	const [ findUser ] = await res.db.execute('SELECT * FROM u__accounts u WHERE u.email= ?', [email]);
	console.log(findUser)
	if (!findUser.length) return authFail;
	if(!await bcrypt.compare(password, findUser[0].password)) return authFail;

	const userData = {
		userId: findUser[0].id,
		email: findUser[0].email,
		permissions: findUser[0].permissions,
		verified: findUser[0].verified
	}
	
	const token = await jwtSign(userData, process.env.JWT_KEY, { expiresIn: process.env.JWT_EXP_TIME });

	return {
		token,
		...userData,
		expiresIn: process.env.JWT_EXP_TIME,
	}
}

exports.get_data = async (req, res, next) => {
	const { userId } = req.userData;
	const sql = `SELECT d.age d_age, d.man d_man, d.height d_height, dw.id dw_id, dw.value dw_value, dw.created_at dw_date, db.id db_id, db.value db_value, db.created_at db_date
		FROM u__data d
		LEFT JOIN u__data_weight dw ON d.user_id = dw.user_id
		LEFT JOIN u__data_bodyfat db ON d.user_id = db.user_id
		WHERE d.user_id= ?`;

	const [getData] = await res.db.execute('SELECT d.age, d.man, d.height FROM u__data d WHERE d.user_id= ?', [userId]);
	if (!getData.length) return [];

	const prepareSql = (phrase, table) => `SELECT ${phrase}.id, ${phrase}.value, ${phrase}.date FROM ${table} ${phrase} WHERE ${phrase}.user_id = ? ORDER BY ${phrase}.date ASC`;
	
	const bfatDataPromise = res.db.execute(prepareSql('db', 'u__data_bodyfat'), [userId]);
	const weightDataPromise = res.db.execute(prepareSql('dw', 'u__data_weight'), [userId]);

	const [[bfData], [weightData]] = await Promise.all([bfatDataPromise, weightDataPromise]);

	return {
		data: {
			...getData[0],
			weight: weightData,
			bodyfat: bfData
		}
	};
}
