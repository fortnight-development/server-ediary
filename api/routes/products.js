const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const asyncHandler = require('../middleware/async-handler');

const multer  = require('multer');
const uploadProduct = multer({ dest: 'static/img/products' });
// const storage = multer.memoryStorage();
// const upload = multer({ storage: storage });

const ProductsController = require('../controllers/products');

router.get('/:productName', checkAuth, asyncHandler(ProductsController.get_products));

router.post('/', checkAuth, uploadProduct.single('image'), asyncHandler(ProductsController.post_product));


/*
router.post('/image-recognition', uploadProduct.single('image'), asyncHandler(ProductsController.post_image_recognition));
router.post('/ilewazy', asyncHandler(ProductsController.post_product_ilewazy));
*/





module.exports = router;
