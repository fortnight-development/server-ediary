const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const asyncHandler = require('../middleware/async-handler');

const TrainingsController = require('../controllers/trainings.js');

// router.get('/:exerciseDate', checkAuth, TrainingsController.post_exercise);

router.get('/exercise-names/:exerciseName', checkAuth, asyncHandler(TrainingsController.get_exercise_names));

router.post('/exercise-name', checkAuth, asyncHandler(TrainingsController.post_exercise_name));

router.patch('/exercise-name/:exerciseNameId', checkAuth, asyncHandler(TrainingsController.patch_exercise_name));

router.delete('/exercise-name/:exerciseNameId', checkAuth, asyncHandler(TrainingsController.delete_exercise_name));


router.get('/:exerciseDate', checkAuth, asyncHandler(TrainingsController.get_exercises));

router.post('/', checkAuth, asyncHandler(TrainingsController.post_exercise));

router.patch('/:exerciseId', checkAuth, asyncHandler(TrainingsController.patch_exercise));

router.delete('/:exerciseId', checkAuth, asyncHandler(TrainingsController.delete_exercise));

router.post('/:exerciseId', checkAuth, asyncHandler(TrainingsController.post_exercise_set));

router.patch('/:exerciseId/all', checkAuth, asyncHandler(TrainingsController.patch_exercise_all_sets));

router.patch('/:exerciseId/:setId', checkAuth, asyncHandler(TrainingsController.patch_exercise_set));

router.delete('/:exerciseId/:setId', checkAuth, asyncHandler(TrainingsController.delete_exercise_set));




module.exports = router;