const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const asyncHandler = require('../middleware/async-handler');

const MeasurementsController = require('../controllers/measurements');

router.get('/', checkAuth, asyncHandler(MeasurementsController.get_general));

router.post('/general', checkAuth, asyncHandler(MeasurementsController.post_general));

router.patch('/general', checkAuth, asyncHandler(MeasurementsController.patch_general));

router.post('/all', checkAuth, asyncHandler(MeasurementsController.post_all));

module.exports = router;

// router.post('/general', checkAuth, asyncHandler())