const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const asyncHandler = require('../middleware/async-handler');

const MealsController = require('../controllers/meals');

router.get('/:mealDate', checkAuth, asyncHandler(MealsController.get_meals_date));

router.post('/', checkAuth, asyncHandler(MealsController.post_meal));

router.patch('/:mealId', checkAuth, asyncHandler(MealsController.patch_meal));

router.delete('/:mealId', checkAuth, asyncHandler(MealsController.delete_meal));

router.post('/:mealId/:productId', checkAuth, asyncHandler(MealsController.post_meal_product));

router.patch('/:mealId/:productId', checkAuth, asyncHandler(MealsController.patch_meal_product));

router.delete('/:mealId/:productId', checkAuth, asyncHandler(MealsController.delete_meal_product));

module.exports = router;
