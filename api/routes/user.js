const express = require('express');
const router = express.Router();
const asyncHandler = require('../middleware/async-handler');
const checkAuth = require('../middleware/check-auth');

const UserControllers = require('../controllers/user');

router.post('/register', asyncHandler(UserControllers.post_register));

router.post('/login', asyncHandler(UserControllers.post_login));


router.get('/data', checkAuth, asyncHandler(UserControllers.get_data));

router.post('/data', checkAuth, asyncHandler(UserControllers.post_data));

router.patch('/data', checkAuth, asyncHandler(UserControllers.patch_data));


router.post('/data/weight', checkAuth, asyncHandler(UserControllers.post_data_weight));

router.patch('/data/weight', checkAuth, asyncHandler(UserControllers.patch_data_weight));

router.delete('/data/weight', checkAuth, asyncHandler(UserControllers.delete_data_weight));


router.post('/data/bodyfat', checkAuth, asyncHandler(UserControllers.post_data_bodyfat));

router.patch('/data/bodyfat', checkAuth, asyncHandler(UserControllers.patch_data_bodyfat));

router.delete('/data/bodyfat', checkAuth, asyncHandler(UserControllers.delete_data_bodyfat));





module.exports = router;