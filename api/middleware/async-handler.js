module.exports = fn => (request, response, next) => new Promise((resolve) => {
	resolve(fn(request, response, next));
})
.then(res => {
	response.db.end();
	if (res == null) return response.status(204).json();
	const status = res.status || 200;
	if (res instanceof Array) {
		return response.status(status).json(res);
	}
	const result = {...res, status: undefined };
	return response.status(status).json(result);
})
.catch(err => {
	response.db.end();
	if (err instanceof Error) {
		next(err);
	} else {
		return response.status(err.status || 500).json(err);
	}
});