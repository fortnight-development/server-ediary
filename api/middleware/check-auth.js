const jwt = require('jsonwebtoken');
const { promisify } = require('util');
const jwtVerify = promisify(jwt.verify);

module.exports = async (request, response, next) => {
	try {
		let token = request.headers.authorization;
		if (!token) throw new Error('Authentication token was not provided');
		const decoded = await jwtVerify(token.split(" ")[1], process.env.JWT_KEY);
		request.userData = decoded;
		next();
	} catch(err) {
		next(err);
	}
}
