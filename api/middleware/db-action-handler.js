module.exports = (actionResult, resourceName, actionName, insertPropName = 'id') => new Promise((resolve, reject) => {
	switch (actionName) {
		case 'patch':
			actionName = 'updated'
			break;
		case 'delete':
			actionName = 'deleted'
			break;
		default: 
			actionName = actionName;
			break;
	}

	if (actionResult.affectedRows == 0) {
		reject({
			status: 404,
			msg: `${resourceName} doesn't exist or you have no permissions for making any changes to it`
		});
	} else if (actionResult.hasOwnProperty('changedRows') && actionResult.changedRows == 0 ) {
		resolve({
			msg: `No changes were made for ${resourceName.charAt(0).toLowerCase() + resourceName.slice(1)}`
		});
	} else if (actionResult.insertId > 0) {
		resolve({
			msg: `${resourceName} was succesfully added with id ${actionResult.insertId}`,
			[insertPropName]: actionResult.insertId
		});
	} else if (actionName) {
		resolve({
			msg: `${resourceName} was succesfully ${actionName}`
		});
	} else {
		resolve();
	}
});