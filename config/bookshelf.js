const knex = require('knex')(require('./knexfile'));
const bookshelf = require('bookshelf')(knex);
bookshelf.plugin('registry');
bookshelf.plugin('bookshelf-camelcase');

module.exports = bookshelf;